package test;

import org.junit.Assert;
import org.junit.Test;
import src.utililties.Day;
import src.utililties.Time;


public class TestTime {
    @Test
    public void TestTimeGetter() {
        Time t1 = new Time(Day.MON, 10, 1);

        Assert.assertEquals(t1.getDay(), Day.MON);
        Assert.assertEquals(t1.getHour(), 10);
        Assert.assertEquals(t1.getMinute(), 1);
    }

    @Test
    public void TestShift() {
        // Later
        Time t1;

        // Shift all later
        t1 = new Time(Day.SUN, 22, 58);
        Time t1Shifted = t1.shift(true, 1, 1, 1);
        Assert.assertEquals(t1Shifted.getDay(), Day.MON);
        Assert.assertEquals(t1Shifted.getHour(), 23);
        Assert.assertEquals(t1Shifted.getMinute(), 59);

        // Shift minute - minute overflow
        t1 = new Time(Day.SUN, 22, 58);
        Time t1ShiftedMinute = t1.shift(true, 0, 0, 2);
        Assert.assertEquals(t1ShiftedMinute.getDay(), Day.SUN);
        Assert.assertEquals(t1ShiftedMinute.getHour(), 23);
        Assert.assertEquals(t1ShiftedMinute.getMinute(), 0);

        // Shift hour - hour overflow
        t1 = new Time(Day.SUN, 22, 58);
        Time t1ShiftedHour = t1.shift(true, 0, 4, 0);
        Assert.assertEquals(t1ShiftedHour.getDay(), Day.MON);
        Assert.assertEquals(t1ShiftedHour.getHour(), 2);
        Assert.assertEquals(t1ShiftedHour.getMinute(), 58);

        // Shift minute & hour - minute & hour overflow
        t1 = new Time(Day.SUN, 22, 58);
        Time t1ShiftedHourMinute = t1.shift(true, 0, 4, 2);
        Assert.assertEquals(t1ShiftedHourMinute.getDay(), Day.MON);
        Assert.assertEquals(t1ShiftedHourMinute.getHour(), 3);
        Assert.assertEquals(t1ShiftedHourMinute.getMinute(), 0);

        // Shift all - all overflow
        t1 = new Time(Day.SUN, 22, 58);
        Time t1ShiftedAll = t1.shift(true, 1, 4, 2);
        Assert.assertEquals(t1ShiftedAll.getDay(), Day.TUES);
        Assert.assertEquals(t1ShiftedAll.getHour(), 3);
        Assert.assertEquals(t1ShiftedAll.getMinute(), 0);


        // Earlier
        Time t2;

        // Shift all ealier
        t2 = new Time(Day.MON, 2, 3);
        Time t2Shifted = t2.shift(false, 1, 1, 1);
        Assert.assertEquals(t2Shifted.getDay(), Day.SUN);
        Assert.assertEquals(t2Shifted.getHour(), 1);
        Assert.assertEquals(t2Shifted.getMinute(), 2);

        // Shift minute - minute underflow
        t2 = new Time(Day.MON, 2, 3);
        Time t2ShiftedMinute = t2.shift(false, 0, 0, 4);
        Assert.assertEquals(t2ShiftedMinute.getDay(), Day.MON);
        Assert.assertEquals(t2ShiftedMinute.getHour(), 1);
        Assert.assertEquals(t2ShiftedMinute.getMinute(), 59);

        // Shift hour - hour underflow
        t2 = new Time(Day.MON, 2, 3);
        Time t2ShiftedHour = t2.shift(false, 0, 3, 0);
        Assert.assertEquals(t2ShiftedHour.getDay(), Day.SUN);
        Assert.assertEquals(t2ShiftedHour.getHour(), 23);
        Assert.assertEquals(t2ShiftedHour.getMinute(), 3);

        // Shift minute & hour - minute & hour underflow
        t2 = new Time(Day.MON, 2, 3);
        Time t2ShiftedHourMinute = t2.shift(false, 0, 3, 4);
        Assert.assertEquals(t2ShiftedHourMinute.getDay(), Day.SUN);
        Assert.assertEquals(t2ShiftedHourMinute.getHour(), 22);
        Assert.assertEquals(t2ShiftedHourMinute.getMinute(), 59);

        // Shift all - all underflow
        t2 = new Time(Day.MON, 2, 3);
        Time t2ShiftedAll = t2.shift(false, 1, 3, 4);
        Assert.assertEquals(t2ShiftedAll.getDay(), Day.SAT);
        Assert.assertEquals(t2ShiftedAll.getHour(), 22);
        Assert.assertEquals(t2ShiftedAll.getMinute(), 59);
    }


}