package test;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import src.utililties.Booking;
import src.utililties.Day;
import src.utililties.Time;
import src.utililties.ClientCB;
import src.servant.FacilityImpl;


import java.net.InetAddress;
import java.util.Calendar;
import java.util.Date;

public class TestClientCB {

    @Test
    public void TestSetExpiry() {
        try {
            int durationDayInt = 1;
            int durationHourInt = 1;
            int durationMinuteInt = 1;

            Calendar calendar = Calendar.getInstance();
            Date now = new Date();

            calendar.setTime(now);
            calendar.add(Calendar.DATE, durationDayInt);
            calendar.add(Calendar.HOUR, durationHourInt);
            calendar.add(Calendar.MINUTE, durationMinuteInt);
            Date later = calendar.getTime();

            System.out.println(calendar.getTime());

        } catch (Exception ex) {
            System.out.println("Timeout error: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void TestLater() {
        try {
            int durationDayInt = 1;
            int durationHourInt = 1;
            int durationMinuteInt = 1;

            Calendar calendar = Calendar.getInstance();
            Date now = new Date();

            calendar.setTime(now);
            calendar.add(Calendar.DATE, durationDayInt);
            calendar.add(Calendar.HOUR, durationHourInt);
            calendar.add(Calendar.MINUTE, durationMinuteInt);
            Date later = calendar.getTime();

            assertEquals(later.after(now), false);

        } catch (Exception ex) {
            System.out.println("Timeout error: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void TestClientCBIsExpiredFunction() {
        try {
            InetAddress localhost = InetAddress.getByName("jenkov.com");

            Calendar calendar = Calendar.getInstance();

            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, 0);
            calendar.add(Calendar.HOUR, 0);
            calendar.add(Calendar.MINUTE, -1);
            Date client1Exp = calendar.getTime();
            ClientCB client1 = new ClientCB(localhost, 678, client1Exp);

            calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, 0);
            calendar.add(Calendar.HOUR, 0);
            calendar.add(Calendar.MINUTE, 3);
            Date client2Exp = calendar.getTime();
            ClientCB client2 = new ClientCB(localhost, 679, client2Exp);

            calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, 1);
            calendar.add(Calendar.HOUR, 1);
            calendar.add(Calendar.MINUTE, 2);
            Date now = calendar.getTime();

            assertEquals(client2.isExpired(), false);

        } catch (Exception ex) {
            System.out.println("Timeout error: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void IsSameTimeMeansBefore() {
        try {
            FacilityImpl fac = new FacilityImpl("lab", 8, 17);
            InetAddress localhost = InetAddress.getByName("jenkov.com");

            Calendar calendar = Calendar.getInstance();

            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, 0);
            calendar.add(Calendar.HOUR, 0);
            calendar.add(Calendar.MINUTE, 0);
            Date client1Exp = calendar.getTime();

            assertEquals(client1Exp.before(new Date()), false);


        } catch (Exception ex) {
            System.out.println("Timeout error: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void IsSameTimeMeansExpired() {
        try {
            FacilityImpl fac = new FacilityImpl("lab", 8, 17);
            InetAddress localhost = InetAddress.getByName("jenkov.com");

            Calendar calendar = Calendar.getInstance();

            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, 0);
            calendar.add(Calendar.HOUR, 0);
            calendar.add(Calendar.MINUTE, 0);
            Date client1Exp = calendar.getTime();
            ClientCB client1Cb = new ClientCB(localhost, 679, client1Exp);
            assertNotEquals(client1Cb.getExpiry(), new Date());


        } catch (Exception ex) {
            System.out.println("Timeout error: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void RemoveExpiredCbs() {
        try {
            FacilityImpl fac = new FacilityImpl("lab", 8, 17);
            InetAddress localhost = InetAddress.getByName("jenkov.com");

            Calendar calendar = Calendar.getInstance();

            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, 0);
            calendar.add(Calendar.HOUR, 0);
            calendar.add(Calendar.MINUTE, 2);
            Date client1Exp = calendar.getTime();
            ClientCB clientCb1 = new ClientCB(localhost, 678, client1Exp);

            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, 0);
            calendar.add(Calendar.HOUR, 0);
            calendar.add(Calendar.MINUTE, -2);
            Date client2Exp = calendar.getTime();
            ClientCB clientCb2 = new ClientCB(localhost, 679, client2Exp);

            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, 0);
            calendar.add(Calendar.HOUR, 0);
            calendar.add(Calendar.MINUTE, -1);
            Date client3Exp = calendar.getTime();
            ClientCB clientCb3 = new ClientCB(localhost, 679, client3Exp);

            fac.register(clientCb1);
            fac.register(clientCb2);
            fac.register(clientCb3);
            assertEquals(fac.callbackList.size(), 3);
            assertEquals(clientCb1.isExpired(), false);
            assertEquals(clientCb2.isExpired(), true);
            fac.updateCbList();
            assertEquals(fac.callbackList.size(), 1);

        } catch (Exception ex) {
            System.out.println("Timeout error: " + ex.getMessage());
        }
    }

}
