package test;

import org.junit.Assert;
import org.junit.Test;
import src.utililties.Booking;
import src.utililties.DailyTimeTable;
import src.utililties.Day;
import src.utililties.Time;


public class TestDailyTimeTable {
    @Test
    public void TestTimeTableGetDay() {
        DailyTimeTable DayTT = new DailyTimeTable(Day.MON);

        Assert.assertEquals(DayTT.getDay(), Day.MON);
    }

    @Test
    public void TestAddGetBookings() {
        Time t1 = new Time(Day.MON, 10, 0);
        Time t2 = new Time(Day.MON, 12, 0);
        Time t3 = new Time(Day.MON, 14, 0);
        Time t4 = new Time(Day.MON, 16, 0);

        // Instatiate TimeTable and Booking object
        DailyTimeTable DayTT = new DailyTimeTable(Day.MON);
        Booking booking1 = new Booking(t1, t2);
        Booking booking2 = new Booking(t3, t4);

        // Should return empty list
        Assert.assertEquals(0, DayTT.getAllBookings().size());

        // Add 2 bookings
        DayTT.addBooking(booking1);
        DayTT.addBooking(booking2);

        // Should return 2 booking
        Assert.assertEquals(2, DayTT.getAllBookings().size());

        // Should be sorted
        Assert.assertEquals(10, DayTT.getAllBookings().get(0).getStartTime().getHour());
        Assert.assertEquals(14, DayTT.getAllBookings().get(1).getStartTime().getHour());
    }

    @Test
    public void TestDeleteBookings() {
        Time t1 = new Time(Day.MON, 10, 0);
        Time t2 = new Time(Day.MON, 12, 0);

        // Instatiate TimeTable and Booking object
        DailyTimeTable DayTT = new DailyTimeTable(Day.MON);
        Booking booking1 = new Booking(t1, t2);

        // Should return empty list
        Assert.assertEquals(0, DayTT.getAllBookings().size());

        // Add 1 bookings
        DayTT.addBooking(booking1);

        // Should return 1 booking
        Assert.assertEquals(1, DayTT.getAllBookings().size());

        // delete 1 bookings
        DayTT.deleteBooking(0);

        // Should return 0 booking
        Assert.assertEquals(0, DayTT.getAllBookings().size());
    }
}