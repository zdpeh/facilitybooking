package test;

import java.util.*;

import org.junit.Test;
import src.servant.BookingResult;
import src.utililties.Booking;
import src.utililties.Day;
import src.utililties.Time;
import src.servant.FacilityImpl;

import static org.junit.Assert.assertEquals;

public class TestFacilityImpl {
    Time mon9 = new Time(Day.MON, 9, 0);
    Time mon10 = new Time(Day.MON, 10, 0);
    Time mon12 = new Time(Day.MON, 12, 0);
    Time mon13 = new Time(Day.MON, 13, 0);
    Time mon14 = new Time(Day.MON, 14, 0);
    Time mon15 = new Time(Day.MON, 15, 0);

    Time tues9 = new Time(Day.TUES, 9, 0);
    Time tues10 = new Time(Day.TUES, 10, 0);
    Time tues11 = new Time(Day.TUES, 11, 0);
    Time tues12 = new Time(Day.TUES, 12, 0);
    Time tues13 = new Time(Day.TUES, 13, 0);
    Time tues14 = new Time(Day.TUES, 14, 0);
    Time tues15 = new Time(Day.TUES, 15, 0);
    Time tues16 = new Time(Day.TUES, 16, 0);


    Time wed9 = new Time(Day.WED, 9, 0);
    Time wed10 = new Time(Day.WED, 10, 0);
    Time wed11 = new Time(Day.WED, 11, 0);
    Time wed12 = new Time(Day.WED, 12, 0);
    Time wed13 = new Time(Day.WED, 13, 0);
    Time wed14 = new Time(Day.WED, 14, 0);
    Time wed15 = new Time(Day.WED, 15, 0);
    Time wed16 = new Time(Day.WED, 16, 0);

    List<Day> allDays = new ArrayList<Day>(EnumSet.allOf(Day.class));
    List<Day> someDays = new ArrayList<Day>();


    FacilityImpl fac = new FacilityImpl("lab", 8, 17);


    @Test
    public void TestHasClashFunction() {
        try {
            fac.makeBooking(tues13, wed16);
            assertEquals(fac.hasClash(wed15, wed16),true);

            System.out.println(fac.getAvailString(someDays));
            // assertEquals(newBooking.getStartTime(), t1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestHasClashFunction2() {
        try {
            fac.makeBooking(tues13, wed15);
            assertEquals(fac.hasClash(wed15, wed16),false);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestHasClashFunction3() {
        try {
            fac.makeBooking(mon10, wed15);
            assertEquals(fac.hasClash(tues9, tues13),true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestHasClashFunctionForExistingBooking() {
        try {
            fac.makeBooking(mon9, wed15);
            assertEquals(fac.hasClash(1, mon10, tues13),false);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestHasClashFunctionForExistingBooking2() {
        try {
            BookingResult bookingResult = fac.makeBooking(mon9, tues12);
            fac.makeBooking(tues12, wed16);
            assertEquals(fac.hasClash(1, mon10, tues11),false);
            assertEquals(fac.hasClash(1, mon10, tues13),true);
            System.out.println(fac.getAvailString(allDays));


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestMakeBooking() {
        try {
            fac.makeBooking(wed15, wed16);
            BookingResult bookingResult1 = fac.makeBooking(tues9, tues13);
            BookingResult bookingResult2 = fac.makeBooking(mon10, mon12);

            System.out.println(fac.getAvailString(someDays));
            // assertEquals(newBooking.getStartTime(), t1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestMakeBookingStartTimeBeforeEndTime() {
        try {
            assertEquals(fac.makeBooking(wed16, wed15), -1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestMakeBookingStartTimeEqualEndTime() {
        try {

            assertEquals(fac.makeBooking(wed15, wed15), -1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestCancelExistingBooking() {
        try {
            fac.makeBooking(wed15, wed16);

            assertEquals(fac.cancelBooking(1), true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestCancelNonExistingBooking() {
        try {
            fac.makeBooking(wed15, wed16);

            assertEquals(fac.cancelBooking(2), false);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestDoubleCancelExistingBooking() {
        try {
            fac.makeBooking(wed15, wed16);

            assertEquals(fac.cancelBooking(1), true);
            assertEquals(fac.cancelBooking(1), false);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestSortBookingPrintout() {
        try {
            fac.makeBooking(wed15, wed16);
            BookingResult bookingResult1 = fac.makeBooking(tues9, tues13);
            BookingResult bookingResult2 = fac.makeBooking(mon10, mon12);




            System.out.println(fac.getAvailString(someDays));
            // assertEquals(newBooking.getStartTime(), t1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestBookingId() {
        try {
            BookingResult bookingResult1 = fac.makeBooking(wed15, wed16);
            BookingResult bookingResult2 = fac.makeBooking(tues9, tues13);
            BookingResult bookingResult3 = fac.makeBooking(mon10, mon12);


            assertEquals(bookingResult1.getBookingId().intValue(), 1);
            assertEquals(bookingResult2.getBookingId().intValue(), 2);
            assertEquals(bookingResult3.getBookingId().intValue(), 3);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestNewBookingClash() {
        try {
            fac.makeBooking(tues13, wed16);
            assertEquals(fac.bookings.size(), 1);

            assertEquals(fac.makeBooking(tues9, wed16), -1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestBackToBackBooking() {
        try {
            BookingResult bookingResult = fac.makeBooking(mon12, wed15);
            assertEquals(fac.makeBooking(wed15, wed16), 2);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestExactClash() {
        try {
            BookingResult bookingResult3 = fac.makeBooking(wed15, wed16);
            assertEquals(fac.makeBooking(wed15, wed16), -1);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestShiftOldBookingNoClashAndChanges() {
        try {
            BookingResult bookingResult3 = fac.makeBooking(mon10, wed10);
            assertEquals(fac.makeBooking(wed14, wed16), 2);
            fac.shiftBooking(1, true, 0, 1, 1);
            assertEquals(fac.getBookingById(1).getStartTime().getDay(), Day.MON);
            assertEquals(fac.getBookingById(1).getStartTime().getHour(), 11);
            assertEquals(fac.getBookingById(1).getStartTime().getMinute(), 1);

            assertEquals(fac.getBookingById(1).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(1).getEndTime().getHour(), 11);
            assertEquals(fac.getBookingById(1).getEndTime().getMinute(), 1);

            assertEquals(fac.getBookingById(2).getStartTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getStartTime().getHour(), 14);
            assertEquals(fac.getBookingById(2).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getEndTime().getHour(), 16);
            assertEquals(fac.getBookingById(2).getEndTime().getMinute(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestShiftOldBookingClashAndStayTheSame() {
        try {
            fac.makeBooking(mon10, wed10);
            fac.makeBooking(wed14, wed16);

            assertEquals(fac.getBookingById(1).getStartTime().getDay(), Day.MON);
            assertEquals(fac.getBookingById(1).getStartTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(1).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(1).getEndTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getEndTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getStartTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getStartTime().getHour(), 14);
            assertEquals(fac.getBookingById(2).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getEndTime().getHour(), 16);
            assertEquals(fac.getBookingById(2).getEndTime().getMinute(), 0);

            fac.shiftBooking(1, true, 1, 3, 1);

            assertEquals(fac.getBookingById(1).getStartTime().getDay(), Day.MON);
            assertEquals(fac.getBookingById(1).getStartTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(1).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(1).getEndTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getEndTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getStartTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getStartTime().getHour(), 14);
            assertEquals(fac.getBookingById(2).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getEndTime().getHour(), 16);
            assertEquals(fac.getBookingById(2).getEndTime().getMinute(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestShiftOldBookingHasCorrectTime() {
        try {
            fac.makeBooking(mon10, wed10);
            System.out.println(fac.getAvailString(allDays));
            fac.shiftBooking(1, true, 1, 4, 1);
            assertEquals(fac.bookings.get(0).getStartTime().getDay(), Day.TUES);
            assertEquals(fac.bookings.get(0).getStartTime().getHour(), 14);
            assertEquals(fac.bookings.get(0).getStartTime().getMinute(), 1);

            assertEquals(fac.bookings.get(0).getEndTime().getDay(), Day.THURS);
            assertEquals(fac.bookings.get(0).getEndTime().getHour(), 14);
            assertEquals(fac.bookings.get(0).getEndTime().getMinute(), 1);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestShiftOldBookingBackwardsHasCorrectTime() {
        try {
            fac.makeBooking(tues10, wed10);
            System.out.println(fac.getAvailString(allDays));
            fac.shiftBooking(1, false, 1, 4, 1);
            assertEquals(fac.bookings.get(0).getStartTime().getDay(), Day.MON);
            assertEquals(fac.bookings.get(0).getStartTime().getHour(), 5);
            assertEquals(fac.bookings.get(0).getStartTime().getMinute(), 59);

            assertEquals(fac.bookings.get(0).getEndTime().getDay(), Day.TUES);
            assertEquals(fac.bookings.get(0).getEndTime().getHour(), 5);
            assertEquals(fac.bookings.get(0).getEndTime().getMinute(), 59);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestShiftOldBookingBackwardsShouldHaveNoClash() {
        try {
            BookingResult bookingResult1 = fac.makeBooking(mon10, wed10);
            BookingResult bookingResult2 = fac.makeBooking(wed15, wed16);
            assertEquals(bookingResult1.getBookingId().intValue(), 1);
            assertEquals(bookingResult1.getBookingId().intValue(), 2);
            fac.shiftBooking(2, false, 0, 1, 1);

            assertEquals(fac.getBookingById(1).getStartTime().getDay(), Day.MON);
            assertEquals(fac.getBookingById(1).getStartTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(1).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(1).getEndTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getEndTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getStartTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getStartTime().getHour(), 13);
            assertEquals(fac.getBookingById(2).getStartTime().getMinute(), 59);

            assertEquals(fac.getBookingById(2).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getEndTime().getHour(), 14);
            assertEquals(fac.getBookingById(2).getEndTime().getMinute(), 59);

            System.out.println(fac.getAvailString(allDays));


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestShiftOldBookingBackClash() {
        try {
            BookingResult bookingResult = fac.makeBooking(mon10, wed15);
            System.out.println(fac.getAvailString(allDays));
            fac.makeBooking(wed15, wed16);
            System.out.println(fac.getAvailString(allDays));
            fac.shiftBooking(2, false, 0, 1, 1);
            System.out.println(fac.getAvailString(allDays));

            assertEquals(fac.getBookingById(1).getStartTime().getDay(), Day.MON);
            assertEquals(fac.getBookingById(1).getStartTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(1).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(1).getEndTime().getHour(), 15);
            assertEquals(fac.getBookingById(1).getEndTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getStartTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getStartTime().getHour(), 15);
            assertEquals(fac.getBookingById(2).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getEndTime().getHour(), 16);
            assertEquals(fac.getBookingById(2).getEndTime().getMinute(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestShiftBookingEarlierThan8() {
        try {
            fac.makeBooking(mon9, mon10);
            assertEquals(fac.shiftBooking(1, false, 0, 1, 1),false);
            System.out.println(fac.getAvailString(allDays));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestShiftBookingLaterThan5pm() {
        try {
            fac.makeBooking(mon9, wed15);
            assertEquals(fac.shiftBooking(1, true, 0, 2, 1),false);
            System.out.println(fac.getAvailString(allDays));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestShiftBookingIntoNextWeek() {
        try {
            fac.makeBooking(wed9, wed15);
            assertEquals(fac.shiftBooking(1, true, 5, 0, 0),false);
            System.out.println(fac.getAvailString(allDays));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestShiftBookingPartiallyIntoPreviousWeek() {
        try {
            fac.makeBooking(mon10, wed15);
            assertEquals(fac.shiftBooking(1, false, 1, 0, 0),false);
            System.out.println(fac.getAvailString(allDays));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestShiftBookingFullyIntoPreviousWeek() {
        try {
            fac.makeBooking(mon10, wed15);
            assertEquals(fac.shiftBooking(1, false, 3, 0, 0),false);
            System.out.println(fac.getAvailString(allDays));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Test
    public void TestEditDurationOldBookingNoClashAndChange() {
        try {
            fac.makeBooking(mon10, wed10);
            assertEquals(fac.makeBooking(wed15, wed16), 2);
            assertEquals(fac.editBookingDuration(1, true, 0, 1, 1),true);

            assertEquals(fac.getBookingById(1).getStartTime().getDay(), Day.MON);
            assertEquals(fac.getBookingById(1).getStartTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(1).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(1).getEndTime().getHour(), 11);
            assertEquals(fac.getBookingById(1).getEndTime().getMinute(), 1);

            assertEquals(fac.getBookingById(2).getStartTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getStartTime().getHour(), 15);
            assertEquals(fac.getBookingById(2).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getEndTime().getHour(), 16);
            assertEquals(fac.getBookingById(2).getEndTime().getMinute(), 0);

            System.out.println(fac.getAvailString(allDays));


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestEditDurationOldBookingClashAndNoChange() {
        try {
            fac.makeBooking(mon10, tues10);
            assertEquals(fac.makeBooking(wed15, wed16), 2);
            assertEquals(fac.editBookingDuration(1, true, 1, 5, 20),false);

            assertEquals(fac.getBookingById(1).getStartTime().getDay(), Day.MON);
            assertEquals(fac.getBookingById(1).getStartTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(1).getEndTime().getDay(), Day.TUES);
            assertEquals(fac.getBookingById(1).getEndTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getEndTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getStartTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getStartTime().getHour(), 15);
            assertEquals(fac.getBookingById(2).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getEndTime().getHour(), 16);
            assertEquals(fac.getBookingById(2).getEndTime().getMinute(), 0);

            System.out.println(fac.getAvailString(allDays));



        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestEditDurationOldBookingShrinkNoClash() {
        try {
            fac.makeBooking(mon10, tues10);
            assertEquals(fac.makeBooking(wed15, wed16), 2);
            assertEquals(fac.editBookingDuration(1, false, 0, 5, 20),true);

            assertEquals(fac.getBookingById(1).getStartTime().getDay(), Day.MON);
            assertEquals(fac.getBookingById(1).getStartTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(1).getEndTime().getDay(), Day.TUES);
            assertEquals(fac.getBookingById(1).getEndTime().getHour(), 4);
            assertEquals(fac.getBookingById(1).getEndTime().getMinute(), 40);

            assertEquals(fac.getBookingById(2).getStartTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getStartTime().getHour(), 15);
            assertEquals(fac.getBookingById(2).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(2).getEndTime().getDay(), Day.WED);
            assertEquals(fac.getBookingById(2).getEndTime().getHour(), 16);
            assertEquals(fac.getBookingById(2).getEndTime().getMinute(), 0);

            System.out.println(fac.getAvailString(allDays));



        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void TestEditDurationEndCannotBeforeStart() {
        try {
            fac.makeBooking(mon10, tues10);
            assertEquals(fac.makeBooking(wed15, wed16), 2);
            assertEquals(fac.editBookingDuration(1, false, 1, 5, 20),false);

            assertEquals(fac.getBookingById(1).getStartTime().getDay(), Day.MON);
            assertEquals(fac.getBookingById(1).getStartTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getStartTime().getMinute(), 0);

            assertEquals(fac.getBookingById(1).getEndTime().getDay(), Day.TUES);
            assertEquals(fac.getBookingById(1).getEndTime().getHour(), 10);
            assertEquals(fac.getBookingById(1).getEndTime().getMinute(), 0);

            System.out.println(fac.getAvailString(allDays));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
