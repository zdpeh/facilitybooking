package test;

import org.junit.Test;
import src.utililties.Booking;
import src.utililties.Day;
import src.utililties.Time;

import static org.junit.Assert.assertEquals;

public class TestBooking {

    Time t1 = new Time(Day.MON, 10, 0);
    Time t2 = new Time(Day.MON, 12, 0);

    Time t3 = new Time(Day.TUES, 9, 0);
    Time t4 = new Time(Day.WED, 15, 0);

    Time t5 = new Time(Day.WED, 15, 0);
    Time t6 = new Time(Day.WED, 16, 0);

    Time t7 = new Time(Day.MON, 9, 0);
    Time t8 = new Time(Day.TUES, 13, 0);


    @Test
    public void TestBookingGetterSetter() {
        Booking newBooking = new Booking(t1, t2);

        // Test getter
        assertEquals(newBooking.getStartTime(), t1);
        assertEquals(newBooking.getEndTime(), t2);

        // Test setter
        newBooking.setStartTime(t3);
        assertEquals(newBooking.getStartTime(), t3);
        newBooking.setEndTime(t4);
        assertEquals(newBooking.getEndTime(), t4);
    }

    @Test
    public void TestShiftBooking() {
        Booking booking = new Booking(t5, t6);

        Booking newBooking = booking.shiftBooking(false, 1, 1, 1);
        Time t5Shifted = new Time(Day.TUES, 13, 59);
        Time t6Shifted = new Time(Day.TUES, 14, 59);

        assertEquals(newBooking.getStartTime().getDay(), t5Shifted.getDay());
        assertEquals(newBooking.getStartTime().getHour(), t5Shifted.getHour());
        assertEquals(newBooking.getStartTime().getMinute(), t5Shifted.getMinute());

        assertEquals(newBooking.getEndTime().getDay(), t6Shifted.getDay());
        assertEquals(newBooking.getEndTime().getHour(), t6Shifted.getHour());
        assertEquals(newBooking.getEndTime().getMinute(), t6Shifted.getMinute());
    }

    @Test
    public void TestEditBookingDuration() {
        Booking booking = new Booking(t7, t8);

        Booking newBooking = booking.editBookingDuration(false, 1, 1, 1);
        Time t7Edited = new Time(Day.MON, 9, 0);
        Time t8Edited = new Time(Day.MON, 11, 59);

        assertEquals(t7Edited.getDay(), newBooking.getStartTime().getDay());
        assertEquals(t7Edited.getHour(), newBooking.getStartTime().getHour());
        assertEquals(t7Edited.getMinute(), newBooking.getStartTime().getMinute());

        assertEquals(t8Edited.getDay(), newBooking.getEndTime().getDay());
        assertEquals(t8Edited.getHour(), newBooking.getEndTime().getHour());
        assertEquals(t8Edited.getMinute(), newBooking.getEndTime().getMinute());
    }

    @Test
    public void TestAddFunction() {
        // Time t7 = new Time(Day.MON, 9, 0);
        // Time t8 = new Time(Day.TUES, 13, 0);
        Booking booking = new Booking(t7, t8);
        Booking newBooking = booking.add(true, 1, 1, 1);
        
        assertEquals(booking.getStartTime().getDay(), Day.MON);
        assertEquals(booking.getStartTime().getHour(), 9);
        assertEquals(booking.getStartTime().getMinute(), 0);

        assertEquals(booking.getEndTime().getDay(), Day.TUES);
        assertEquals(booking.getEndTime().getHour(), 13);
        assertEquals(booking.getEndTime().getMinute(), 0);

        assertEquals(newBooking.getStartTime().getDay(), Day.TUES);
        assertEquals(newBooking.getStartTime().getHour(), 10);
        assertEquals(newBooking.getStartTime().getMinute(), 1);

        assertEquals(newBooking.getEndTime().getDay(), Day.WED);
        assertEquals(newBooking.getEndTime().getHour(), 14);
        assertEquals(newBooking.getEndTime().getMinute(), 1);

        
    }

    @Test
    public void TestBeforeAfterAddFunction() {
        Time t10 = new Time(Day.MON, 13, 0);
        Time t11 = new Time(Day.WED, 14, 0);
        Booking booking = new Booking(t10, t11);
        Booking newBooking = booking.add(false, 1, 1, 1);

        assertEquals(newBooking.getStartTime().isBefore(booking.getStartTime()), true);
        
    }
    


}
