package src.servant;

import src.utililties.*;

import java.rmi.*;
import java.util.*;

interface FacilityInterface extends Remote {
    public String getAvailString(List<Day> days) throws RemoteException;
    public BookingResult makeBooking(Time startTime, Time endTime) throws RemoteException;
    public boolean cancelBooking(int bookingId) throws RemoteException;
    public ModificationResult shiftBooking(int bookingId, boolean forward, int day, int hour, int minute) throws RemoteException;
    public ModificationResult editBookingDuration(int bookingId, boolean forward, int day, int hour, int minute) throws RemoteException;
    public boolean register(ClientCB cb) throws RemoteException;

}