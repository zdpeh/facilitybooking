package src.servant;

import src.utililties.*;


import java.rmi.*;
import java.util.*;


public class FacilityImpl implements FacilityInterface {
    public String facilityName;
    public List<Booking> bookings;
    public List<ClientCB> callbackList;
    public int facStartHour;
    public int facEndHour;

    // FacilityImpl object constructor
    public FacilityImpl(String facilityName, int facStartHour, int facEndHour) {
        super();
        this.facilityName = facilityName;
        this.facStartHour = facStartHour;
        this.facEndHour = facEndHour;
        this.bookings = new Vector<Booking>();
        this.callbackList = new Vector<ClientCB>();
    }

    /**
     * Function to print availability of facility for given days
     *
     * @return void
     */
    public String getAvailString(List<Day> days) throws RemoteException { //tested
        DailyTimeTable weeklyTimeTable[] = {
                new DailyTimeTable(Day.MON),
                new DailyTimeTable(Day.TUES),
                new DailyTimeTable(Day.WED),
                new DailyTimeTable(Day.THURS),
                new DailyTimeTable(Day.FRI),
                new DailyTimeTable(Day.SAT),
                new DailyTimeTable(Day.SUN),
        };


        Iterator<Booking> it = this.bookings.iterator();
        while (it.hasNext()) {
            Booking bk = it.next();
            if (bk.getStartTime().getDay() == bk.getEndTime().getDay()) {
                Booking newBooking = new Booking(bk.getStartTime(), bk.getEndTime());
                weeklyTimeTable[bk.getStartTime().getDay().ordinal()].addBooking(newBooking);
            } else { // if the booking is across more than 1 day, need to split up the booking over multiple days
                int noOfDays = bk.getEndTime().getDay().ordinal() - bk.getStartTime().getDay().ordinal() - 1;
                for (int i = 1; i <= noOfDays; i++) {
                    Time t1 = new Time(bk.getStartTime().getDay().next(i), this.facStartHour, 0);
                    Time t2 = new Time(bk.getStartTime().getDay().next(i), this.facEndHour, 0);
                    Booking newBooking = new Booking(t1, t2);
                    weeklyTimeTable[t1.getDay().ordinal()].addBooking(newBooking);
                }

                Booking newBooking1 = new Booking(bk.getStartTime(), new Time(bk.getStartTime().getDay(), this.facEndHour, 0));
                Booking newBooking2 = new Booking(new Time(bk.getEndTime().getDay(), this.facStartHour, 0), bk.getEndTime());
                weeklyTimeTable[bk.getStartTime().getDay().ordinal()].addBooking(newBooking1);
                weeklyTimeTable[bk.getEndTime().getDay().ordinal()].addBooking(newBooking2);
            }

        }
        StringBuilder retString = new StringBuilder();

        retString.append(days).append("\n");
        for (DailyTimeTable dtt : weeklyTimeTable) {
            if (days.contains(dtt.getDay())) {
                List<Booking> bookings = dtt.getAllBookings();
                Iterator<Booking> it2 = bookings.iterator();
                while (it2.hasNext()) {
                    Booking bk = it2.next();
                    retString.append(bk.getStartTime().verbose()).append(" ").append(bk.getEndTime().verbose()).append("\n");
                }
            }
        }

        return retString.toString();
    }

    /**
     * Helper function to check if startTime and endTime has any clash with existing bookings
     *
     * @return Boolean, true if clash, false if no clash
     */
    public boolean hasClash(Time startTime, Time endTime) throws RemoteException { //tested
        Iterator<Booking> it = this.bookings.iterator();
        while (it.hasNext()) {
            Booking booking = it.next();
            // check if startTime or endTime is in between existing booking
            if (startTime.isEqual(booking.getStartTime()) && endTime.isEqual(booking.getEndTime())) {
                System.out.println("exact clash!");
                return true;
            }

            if (startTime.isBetween(booking.getStartTime(), booking.getEndTime()) || endTime.isBetween(booking.getStartTime(), booking.getEndTime())) {
                System.out.println("clashdsfsdfgsdfsg!");
                return true;
            }

            if (booking.getStartTime().isBetween(startTime, endTime) || booking.getEndTime().isBetween(startTime, endTime)) {
                System.out.println("clashhjj");
                return true;
            }
        }

        return false;
    }

    /**
     * Overloaded Helper function to check if startTime and endTime has any clash with existing bookings
     *
     * @return Boolean, true if clash, false if no clash
     */
    public boolean hasClash(int bookingId, Time startTime, Time endTime) throws RemoteException { //tested
        Iterator<Booking> it = this.bookings.iterator();
        while (it.hasNext()) {
            Booking booking = it.next();
            // check if startTime or endTime is in between existing booking
            if (bookingId == booking.getBookingId()) {
                continue;
            }
            if (startTime.isBetween(booking.getStartTime(), booking.getEndTime()) || endTime.isBetween(booking.getStartTime(), booking.getEndTime())) {
                System.out.println("asdaclash!");
                return true;
            }

            if (booking.getStartTime().isBetween(startTime, endTime) || booking.getEndTime().isBetween(startTime, endTime)) {
                System.out.println("clfdfdash");
                return true;
            }
        }

        return false;
    }

    /**
     * Function to make a new booking
     *
     * @return int, ID of new booking, -1 if failed
     */
    public BookingResult makeBooking(Time startTime, Time endTime) throws RemoteException { //tested
        // check that startTime is before endTime
        if (endTime.isBefore(startTime)) {
            String errorMessage = "Booking Unsuccessful! End time before start time.";
            System.out.println(errorMessage);
            BookingResult res = new BookingResult(errorMessage, false, -1);
            return res;
        }

        // Ending time == start time
        if (endTime.isEqual(startTime)) {
            String errorMessage = "Booking Unsuccessful! End time equal to start time.";
            System.out.println(errorMessage);
            BookingResult res = new BookingResult(errorMessage, false, -1);
            return res;
        }

        // Start time is before office hours
        if (startTime.getHour() < this.facStartHour) {
            String errorMessage = "Booking Unsuccessful! Start time is earlier than office hours!";
            System.out.println(errorMessage);
            BookingResult res = new BookingResult(errorMessage, false, -1);
            return res;
        }

        // End time is past office hours
        if (endTime.getHour() == this.facEndHour && endTime.getMinute() > 0 || endTime.getHour() > this.facEndHour) {
            String errorMessage = "Booking Unsuccessful! End time is past office hours!";
            System.out.println(errorMessage);
            BookingResult res = new BookingResult(errorMessage, false, -1);
            return res;
        }

        // Check if there are clashes on start day and/or end day
        if (this.hasClash(startTime, endTime)) {
            String errorMessage = "Booking Unsuccessful! Clash with existing booking.";
            System.out.println(errorMessage);
            BookingResult res = new BookingResult(errorMessage, false, -1);
            return res;
        } else {
            Booking.increaseBookingCounter();
            Booking newBooking = new Booking(startTime, endTime);
            int id = newBooking.getBookingId();
            this.bookings.add(newBooking);

            // Return results
            String errorMessage = "Booking Successful! Booking ID: " + id;
            System.out.println(errorMessage);
            BookingResult res = new BookingResult(errorMessage, true, id);
            return res;
        }

    }

    /**
     * Function to cancel bookingId booking
     *
     * @return Boolean, True if cancelled, False if failed
     */
    public boolean cancelBooking(int bookingId) throws RemoteException { //tested
        Iterator<Booking> it = this.bookings.iterator();
        while (it.hasNext()) {
            if (it.next().getBookingId() == bookingId) {
                it.remove();
                return true;
            }
        }

        return false;
    }

    /**
     * Function to shift bookingId booking by offset
     *
     * @return Boolean, True if cancelled, False if failed
     */
    public ModificationResult shiftBooking(int bookingId, boolean forward, int day, int hour, int minute) throws RemoteException { //tested
        Iterator<Booking> it = this.bookings.iterator();
        while (it.hasNext()) {
            Booking oldBk = it.next();
            if (oldBk.getBookingId() == bookingId) { //if booking id exist, check for clash
                Booking newBk = oldBk.add(forward, day, hour, minute);
                if (newBk == null) {
                    String errorMessage = "End time before start time (i.e. shift has spilt over into other weeks) OR operators out of range";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                }

                if (newBk.getEndTime().getHour() > this.facEndHour) {
                    String errorMessage = "End time is past office hours!";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                }

                if (newBk.getEndTime().getHour() == this.facEndHour && newBk.getEndTime().getMinute() > 0) {
                    String errorMessage = "End time is past office hours!";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                }

                if (newBk.getStartTime().getHour() < this.facStartHour) {
                    String errorMessage = "Start time is earlier than office hours!";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                }

                if (forward && newBk.getStartTime().isBefore(oldBk.getStartTime()) && newBk.getEndTime().isBefore(oldBk.getEndTime())) {
                    String errorMessage = "Booking will spill over into next week when shifted forward!";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                }

                if (!forward && newBk.getStartTime().isAfter(oldBk.getStartTime()) && newBk.getEndTime().isAfter(oldBk.getEndTime())) {
                    String errorMessage = "Booking will spill over into previous week when shifted backward!";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                }

                if (this.hasClash(oldBk.getBookingId(), newBk.getStartTime(), newBk.getEndTime())) {
                    String errorMessage = "Clash with existing booking!";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                } else {
                    oldBk.setStartTime(newBk.getStartTime());
                    oldBk.setEndTime(newBk.getEndTime());
                    String errorMessage = "Booking successfully shifted";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, true);
                    return res;
                }
            }
        }

        String errorMessage = "Booking id not found";
        System.out.println(errorMessage);
        ModificationResult res = new ModificationResult(errorMessage, false);
        return res;
    }

    public ModificationResult editBookingDuration(int bookingId, boolean forward, int day, int hour, int minute) throws RemoteException { //tested
        Iterator<Booking> it = this.bookings.iterator();
        while (it.hasNext()) {
            Booking oldBk = it.next();
            if (oldBk.getBookingId() == bookingId) { //if booking id exist, check for clash
                Booking newBk = oldBk.editBookingDuration(forward, day, hour, minute);
                if (newBk == null) {
                    String errorMessage = "End time before start time OR End time overflowed to past/previous week!";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                }

                if (newBk.getEndTime().getHour() > this.facEndHour) {
                    String errorMessage = "End time is past office hours!";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                }

                if (newBk.getEndTime().getHour() == this.facEndHour && newBk.getEndTime().getMinute() > 0) {
                    String errorMessage = "End time is past office hours!";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                }

                if (newBk.getStartTime().getHour() < this.facStartHour) {
                    String errorMessage = "Start time is earlier than office hours!";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                }

                if (this.hasClash(oldBk.getBookingId(), newBk.getStartTime(), newBk.getEndTime())) {
                    String errorMessage = "Editing duration has clash!";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, false);
                    return res;
                } else {
                    oldBk.setStartTime(newBk.getStartTime());
                    oldBk.setEndTime(newBk.getEndTime());
                    String errorMessage = "Booking duration changed";
                    System.out.println(errorMessage);
                    ModificationResult res = new ModificationResult(errorMessage, true);
                    return res;
                }

            }
        }
        String errorMessage = "Booking id not found";
        System.out.println(errorMessage);
        ModificationResult res = new ModificationResult(errorMessage, false);
        return res;

    }

    public boolean register(ClientCB cb) throws RemoteException { //tested
        System.out.println("Facility registering client for callback: " + cb.getClientAddress() + "/" + cb.getClientPort());
        this.callbackList.add(cb);
        System.out.println("Current client callbacks registered:");
        for (ClientCB cbb : this.callbackList) {
            System.out.println(" - " + cbb.getClientPort());
        }
        return true;
    }

    public Booking getBookingById(int id) throws RemoteException { // works fine
        Iterator<Booking> it = this.bookings.iterator();
        while (it.hasNext()) {
            Booking booking = it.next();
            if (booking.getBookingId() == id) return booking;
        }

        return null;
    }

    public void updateCbList() throws RemoteException { //tested
        System.out.println("Current client callbacks registered:");
        for (ClientCB cb : this.callbackList) {
            System.out.println(" - " + cb.getClientPort());
        }

        ListIterator<ClientCB> it = this.callbackList.listIterator();
        while (it.hasNext()) {
            ClientCB cb = it.next();
            if (cb.isExpired()) {
                System.out.println("removing one expired callback...");
                it.remove();
            }
        }
    }
}
