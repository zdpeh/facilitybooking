package src.servant;

public final class BookingResult {
    private final String message;
    private final Boolean success;
    private final Integer bookingId;

    public BookingResult(String message, Boolean success, Integer bookingId) {
        this.message = message;
        this.success = success;
        this.bookingId = bookingId;
    }

    public String getMessage() {
        return message;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public Boolean getSuccess() {
        return success;
    }
}
