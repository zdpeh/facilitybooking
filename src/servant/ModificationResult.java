package src.servant;

public final class ModificationResult {
    private final String message;
    private final Boolean success;

    public ModificationResult(String message, Boolean success) {
        this.message = message;
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public Boolean getSuccess() {
        return success;
    }
}

