package src.client;

import src.utililties.Day;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;


public class FacilityClient {
    public static void main(String[] args) {
        // If not enough arguments
        if (args.length < 2) {
            System.out.println("Syntax: QuoteClient <hostname> <port>");
            return;
        }

        // First argument is raw IP address
        String rawAddress = args[0];

        // Second argument is port
        int port = Integer.parseInt(args[1]);

        try {
            // Returns the IP address of a host, given the host's name
            InetAddress address = InetAddress.getByName(rawAddress);

            // Instantiates a new datagram socket
            DatagramSocket socket = new DatagramSocket();
            // Set socket timeout in milliseconds
            socket.setSoTimeout(1000);

            while (true) {
                // Set threshold to percentage of lossy dropoff, e.g. 0.2 == 20% of messages will drop off.
                double threshold = 0.5;
                // Reading data from console
                // BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                Scanner sc = new Scanner(System.in);
                System.out.println(Constants.CLIENTPROMPT);

                // String fromClientConsole = reader.readLine();
                String fromClientConsole = sc.nextLine();
                if (fromClientConsole.equalsIgnoreCase("7")) {
                    System.out.println("Terminating client now...");
                    socket.close();
                    break;
                }


                //Generate a unique ID for the request message
                Random r = new Random(System.currentTimeMillis());
                String requestId = String.valueOf(((1 + r.nextInt(2)) * 10000 + r.nextInt(10000)));

                String reqIDFromClientConsole = requestId + " " + fromClientConsole;

                // Instantiate a new DatagramPacket
                byte[] requestBuffer = reqIDFromClientConsole.getBytes();
                System.out.println("[Request ID: " + requestId + "] Request from Client - " + fromClientConsole);

                DatagramPacket request = new DatagramPacket(requestBuffer, requestBuffer.length, address, port);

                // Random var used to simulate lossy communication
                double prob = Math.random();
                // Send request
                if (prob >= threshold) {
                    socket.send(request);
                }

                // Receive response
                byte[] responseBuffer = new byte[512];
                DatagramPacket response = new DatagramPacket(responseBuffer, responseBuffer.length);

                // At-least-once semantics
                while (true) {
                    try {
                        socket.receive(response);
                        break;
                    } catch (SocketTimeoutException e) {
                        // Resend
                        System.out.println("Socket timeout, resending request...");

                        // Random var used to simulate lossy communication
                        prob = Math.random();
                        if (prob >= threshold) {
                            socket.send(request);
                            System.out.println("Successfully sent request.");
                        }
                    }
                }

                // Parse buffer to string
                String responseFromServer = new String(responseBuffer, 0, response.getLength());
                System.out.println("\nReceived response from server: " + responseFromServer);

                String[] fromClientConsoleComponents = fromClientConsole.split(" ");
                if (fromClientConsoleComponents[0].equals("4")) {
                    System.out.println("Entering monitoring mode...");
                    int durationDayInt = Integer.parseInt(fromClientConsoleComponents[2]);
                    int durationHourInt = Integer.parseInt(fromClientConsoleComponents[3]);
                    int durationMinuteInt = Integer.parseInt(fromClientConsoleComponents[4]);
                    Calendar calendar = Calendar.getInstance();
                    Date now = new Date();
                    calendar.setTime(now);
                    calendar.add(Calendar.DATE, durationDayInt);
                    calendar.add(Calendar.HOUR, durationHourInt);
                    calendar.add(Calendar.MINUTE, durationMinuteInt);
                    Date monitorExpiryDate = calendar.getTime();

                    byte[] monitorResponseBuffer = new byte[1024];
                    DatagramPacket monitorResponse = new DatagramPacket(monitorResponseBuffer, monitorResponseBuffer.length);

                    while (true) {
                        now = new Date(); //update now
                        if (monitorExpiryDate.before(now)) break; //if expired, stop monitoring
                        try {
                            socket.receive(monitorResponse);
                            // Parse buffer to string
                            String update = new String(monitorResponseBuffer, 0, monitorResponse.getLength());
                            System.out.println("\nReceived response from server: " + update);
                        } catch (SocketTimeoutException e) {
                        }


                    }

                    //once we are at this part of the code, means monitoring period has ended. time to allow user to make other requests
                    System.out.println("leaving monitoring mode...");
                    continue;
                }
            }

        } catch (SocketTimeoutException ex) {
            System.out.println("Timeout error: " + ex.getMessage());
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Client error: " + ex.getMessage());
            ex.printStackTrace();
        }
    }



}