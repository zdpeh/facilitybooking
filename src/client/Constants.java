package src.client;

public final class Constants {

    private Constants() {
        // restrict instantiation
    }

    public static final String CLIENTPROMPT =
            "\nPlease choose one service and enter the number: " +
            "\n(1) Check availability of facility - view booked slots: <1> <facility name> <[ array of days ]>, e.g: '1 SRC [MON TUES WED THURS FRI]'" +
            "\n(2) Book a facility: <2> <facility name> <start day> <start hour> <start minute> <end day> <end hour> <end minute>, e.g: '2 SRC MON 10 30 WED 15 45'" +
            "\n(3) Change booking: <3> <booking id> <forward/backward> <offset day> <offset hour> <offset minute>, e.g: '3 1 forward 1 1 1'" +
            "\n(4) Monitor facility: <4> <facility name> <day> <hour> <minute>, e.g: '4 SRC 1 10 10'" +
            "\n(5) Cancel booking: <5> <booking id>, e.g: '5 1'" +
            "\n(6) Extend/shrink booking duration: <6> <confirmation id> <forward/backward> <offset day> <offset hour> <offset minute>, e.g: '6 1 backward 1 1 2'" +
            "\n(7) Exit";
}
