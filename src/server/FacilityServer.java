package src.server;

import src.utililties.Day;
import src.servant.FacilityImpl;
import src.servant.ModificationResult;
import src.servant.BookingResult;
import src.utililties.Time;
import src.utililties.ClientCB;

import java.io.*;
import java.net.*;
import java.rmi.RemoteException;
import java.util.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;


import java.util.Date;
import java.util.Calendar;

enum Facilities {
    NORTHHILL,
    SRC,
    SOFTWARELAB
}

public class FacilityServer {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Too few arguments");
            return;
        }

        // Save port
        int port = Integer.parseInt(args[0]);
        // Invocation semantics, either amo or alo
        String sem = args[1];

        try {
            // Instantiates server
            UDPServer server = new UDPServer(port, sem);
            System.out.println("Server started listening on port " + port);

            // Instantiates facilities
            FacilityImpl f1 = new FacilityImpl(Facilities.SRC.name(), 8, 17);
            FacilityImpl f2 = new FacilityImpl(Facilities.NORTHHILL.name(), 8, 17);
            FacilityImpl f3 = new FacilityImpl(Facilities.SOFTWARELAB.name(), 8, 17);

            // creating a facility Dictionary
            Hashtable<String, FacilityImpl> facDict = new Hashtable<>(); //maps facility name strings to facilityImpl objects
            Hashtable<String, String> requestHistory = new Hashtable<>(); //store the messages of requests that have been processed (for at-most-once mode)
            Hashtable<Integer, FacilityImpl> bookingIdFacDict = new Hashtable<>(); //maps booking ids to the facility object that was booked

            facDict.put(Facilities.SRC.name(), f1);
            facDict.put(Facilities.NORTHHILL.name(), f2);
            facDict.put(Facilities.SOFTWARELAB.name(), f3);

            // Server function
            server.service(facDict, requestHistory, bookingIdFacDict);

        } catch (SocketException ex) {
            System.out.println("Socket error: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("I/O error: " + ex.getMessage());
        }
    }
}


class UDPServer {
    public UDPServer(int port, String sem) throws SocketException {
        // Creates a UDP server listening on port <port>
        socket = new DatagramSocket(port);
        semantics = sem;
    }

    private DatagramSocket socket;
    String semantics;
    int counter = 0;

    public static void main(String[] args) {
    }

    void service(Hashtable<String, FacilityImpl> facDict, Hashtable<String, String> requestHistory, Hashtable<Integer, FacilityImpl> bookingIdFacDict) throws IOException {
        while (true) {
            counter++;
            // Random var used to simulate lossy communication
            double prob = Math.random();
            // Set threshold to percentage of lossy dropoff, e.g. 0.2 == 20% of messages will drop off.
            double threshold = 0.5;
            // (Blocked) Waits on client's request
            byte[] requestBuffer = new byte[512];
            DatagramPacket request = new DatagramPacket(requestBuffer, requestBuffer.length);
            socket.receive(request);

            // Obtain address and port for response
            InetAddress clientAddress = request.getAddress();
            int clientPort = request.getPort();

            // Parse buffer to string
            String requestFromClient = new String(requestBuffer, 0, request.getLength());

            System.out.println("Received request from client: " + requestFromClient);

            // Process request
            String processedData;
            try {
                processedData = processRequest(socket, requestFromClient, facDict, requestHistory, bookingIdFacDict, clientAddress, clientPort);
            } catch (ArrayIndexOutOfBoundsException ex) {
                processedData = "Request has invalid structure - " + ex;
                System.out.println("Client:" + clientAddress + "/" + clientPort + "\t\t\tError:" + processedData);
            } catch (IllegalArgumentException ex) {
                processedData = "Request has invalid arguments - " + ex;
                System.out.println("Client:" + clientAddress + "/" + clientPort + "\t\t\tError:" + processedData);
            }

            // Encodes string into a sequence of bytes
            byte[] responseBuffer = processedData.getBytes();

            // No loss in packets
            if (prob >= threshold) {
                // Send DatagramPacket
                DatagramPacket response = new DatagramPacket(responseBuffer, responseBuffer.length, clientAddress, clientPort);
                socket.send(response);
                System.out.println("Successfully sent response to client. Number of tries: " + counter + "\tResponse sent to client: '" + processedData + "'");
                System.out.println("__________________________________________________________");

                // Reset counter
                counter = 0;
            } else {
                System.out.println("Response data failed to reach client due to lossy connection (simulated).");
            }

            System.out.println("");
        }
    }

    private void informCallbackList(DatagramSocket socket, FacilityImpl facility) throws RemoteException, IOException {
        facility.updateCbList(); //update callback list to ensure that expired callbacks are removed
        List<ClientCB> callbackList = facility.callbackList; //get the list of callback objects monitoring the facility
        Iterator<ClientCB> it = callbackList.iterator();
        List<Day> allDays = new ArrayList<Day>(EnumSet.allOf(Day.class));
        System.out.println(allDays);

        // Create message to be sent to callbacks
        String updatedAvailString = facility.getAvailString(allDays);
        String responseToClient = "Someone has made a booking in the facility you are monitoring, here's the updated list of bookings: " + updatedAvailString;
        System.out.println("Message to be sent to all callbacks: " + responseToClient + "\n");


        System.out.println("Informing all callback...");
        while (it.hasNext()) { // for each callback object in the callback list, send the availability string (in bytes) to the client addr and port
            ClientCB cb = it.next();
            InetAddress clientAddress = cb.getClientAddress();
            int clientPort = cb.getClientPort();
            System.out.println(" - sending to client " + clientAddress + "/" + clientPort);

            byte[] updatedAvailBytes = responseToClient.getBytes();
            DatagramPacket update = new DatagramPacket(updatedAvailBytes, updatedAvailBytes.length, clientAddress, clientPort);
            socket.send(update);
        }
    }

    private String processRequest(DatagramSocket socket, String fromClient, Hashtable<String, FacilityImpl> facDict,
                                  Hashtable<String, String> requestHistory, Hashtable<Integer, FacilityImpl> bookingIdFacDict,
                                  InetAddress clientAddress, int clientPort) throws RemoteException, IOException {
        String[] requestComponents = fromClient.split(" ", 2);
        String requestId = requestComponents[0];
        String clientRequestString = requestComponents[1];
        String clientRequestArray[] = clientRequestString.split(" ", 2);
        String requestType = clientRequestArray[0];
        String requestInfo = clientRequestArray[1];
        String result = "";
        String infoArray[];

        //Filter duplicate requests for amo
        if (semantics.equalsIgnoreCase("amo")) {
            if (requestHistory.containsKey(requestId)) {
                return requestHistory.get(requestId);
            }
        }

        switch (requestType) {
            case "1":
                System.out.println("\n ==== Checking availability ====");
                infoArray = requestInfo.split(" ", 2);
                String facNameString = infoArray[0];
                FacilityImpl facility = facDict.get(facNameString);
                if (facility == null) {
                    result = "Facility not found";
                    break;
                }

                String daysString = infoArray[1];
                String[] daysStringArray = daysString.substring(1, daysString.length() - 1).split(" ");
                List<Day> daysEnumArray = new Vector<Day>();
                for (String dayString : daysStringArray) {
                    daysEnumArray.add(Day.valueOf(dayString));
                }
                result = facility.getAvailString(daysEnumArray);
                break;

            case "2":
                System.out.println("\n ==== Make booking ====");
                infoArray = requestInfo.split(" ");
                facNameString = infoArray[0];
                facility = facDict.get(facNameString);
                if (facility == null) {
                    result = "Facility not found";
                    break;
                }

                Day startDayEnum = Day.valueOf(infoArray[1]);
                int startHourInt = Integer.parseInt(infoArray[2]);
                int startMinuteInt = Integer.parseInt(infoArray[3]);

                Day endDayEnum = Day.valueOf(infoArray[4]);
                int endHourInt = Integer.parseInt(infoArray[5]);
                int endMinuteInt = Integer.parseInt(infoArray[6]);

                BookingResult bookingResult = facility.makeBooking(new Time(startDayEnum, startHourInt, startMinuteInt), new Time(endDayEnum, endHourInt, endMinuteInt));

                bookingIdFacDict.put(bookingResult.getBookingId(), facility);

                // Booking succesful
                if (bookingResult.getSuccess()) {
                    this.informCallbackList(socket, facility); //inform the callback list of facility
                }

                // Return result string
                result = bookingResult.getMessage();

                break;

            case "3":
                System.out.println("\n ==== Shift Booking ====");
                infoArray = requestInfo.split(" ");
                int bookingIdInt = Integer.parseInt(infoArray[0]);
                String direction = infoArray[1];
                int offsetDayInt = Integer.parseInt(infoArray[2]);
                int offsetHourInt = Integer.parseInt(infoArray[3]);
                int offsetMinuteInt = Integer.parseInt(infoArray[4]);
                boolean forward;
                if (direction.equalsIgnoreCase("backward")) forward = false;
                else {
                    if (direction.equalsIgnoreCase("forward")) forward = true;

                    else {
                        System.out.println("wrong argument");
                        break;
                    }
                }

                facility = bookingIdFacDict.get(bookingIdInt);
                if (facility == null) {
                    result = "Booking ID not found";
                    break;
                }

                // Attempt to shift booking
                ModificationResult modificationResult = facility.shiftBooking(bookingIdInt, forward, offsetDayInt, offsetHourInt, offsetMinuteInt);

                // Is successfully shifted booking, inform all callbacks
                if (modificationResult.getSuccess()) {
                    this.informCallbackList(socket, facility); //inform the callback list of facility
                }

                // Return result string
                result = modificationResult.getMessage();

                break;

            case "4":
                System.out.println("\n ==== Monitor facility ====");
                infoArray = requestInfo.split(" ");
                facNameString = infoArray[0];
                int durationDayInt = Integer.parseInt(infoArray[1]);
                int durationHourInt = Integer.parseInt(infoArray[2]);
                int durationMinuteInt = Integer.parseInt(infoArray[3]);

                Calendar calendar = Calendar.getInstance();
                Date now = new Date();
                calendar.setTime(now);
                calendar.add(Calendar.DATE, durationDayInt);
                calendar.add(Calendar.HOUR, durationHourInt);
                calendar.add(Calendar.MINUTE, durationMinuteInt);

                ClientCB clientCB = new ClientCB(clientAddress, clientPort, calendar.getTime());
                facility = facDict.get(facNameString);

                if (facility == null) {
                    result = "Facility not found";
                    break;
                } else {
                    facility.register(clientCB);
                    result = "Registering for monitoring successful";
                }

                break;

            case "5":
                System.out.println("\n ==== Cancel booking ====");
                bookingIdInt = Integer.parseInt(requestInfo);
                System.out.println(bookingIdInt);
                facility = bookingIdFacDict.get(bookingIdInt);
                if (facility == null) {
                    result = "Booking ID not found";
                    break;
                }
                boolean cancelled = facility.cancelBooking(bookingIdInt);

                if (cancelled) {
                    this.informCallbackList(socket, facility); //inform the callback list of facility
                    result = "Booking successfully cancelled!";
                } else {
                    result = "Booking cancellation unsuccessful!";
                }

                break;

            case "6":
                System.out.println("\n ==== Extend/shrink booking duration ====");
                infoArray = requestInfo.split(" ");
                bookingIdInt = Integer.parseInt(infoArray[0]);
                direction = infoArray[1];
                offsetDayInt = Integer.parseInt(infoArray[2]);
                offsetHourInt = Integer.parseInt(infoArray[3]);
                offsetMinuteInt = Integer.parseInt(infoArray[4]);
                boolean extend;
                if (direction.equalsIgnoreCase("backward")) extend = false;
                else {
                    if (direction.equalsIgnoreCase("forward")) extend = true;

                    else {
                        System.out.println("wrong argument");
                        break;
                    }
                }

                facility = bookingIdFacDict.get(bookingIdInt);
                if (facility == null) {
                    result = "Booking ID not found";
                    break;
                }
                // Attempt to edit duration
                ModificationResult extended = facility.editBookingDuration(bookingIdInt, extend, offsetDayInt, offsetHourInt, offsetMinuteInt);

                // Successfully edited duration
                if (extended.getSuccess()) {
                    this.informCallbackList(socket, facility); //inform the callback list of facility
                }

                // Return result string
                result = extended.getMessage();
                break;

        }

        // Store requestID-result pair in requestHistory hashtable
        requestHistory.put(requestId, result);
        return result;
    }
}

