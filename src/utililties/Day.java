package src.utililties;

public enum Day {
    MON,
    TUES,
    WED,
    THURS,
    FRI,
    SAT,
    SUN;

    public static Day[] vals = values();

    /**
    * Function to get next day enum
    *
    * @return Day enum
    */
    public Day next(int count) {
        return vals[(this.ordinal()+count) % vals.length];
    }

    /**
    * Function to get previous day enum
    *
    * @return Day enum
    */
    public Day previous(int count){
        return vals[(7+this.ordinal()-count) % vals.length];
    }
}
