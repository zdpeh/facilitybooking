package src.utililties;
// import java.rmi.*;
// import java.rmi.server.*;

public class Booking {
    static int uniqueIdCounter = 0;
    public int bookingId;
    Time startTime;
    Time endTime;

    // Booking object constructor
    public Booking(Time startTime, Time endTime) {
        this.bookingId = uniqueIdCounter;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public static int increaseBookingCounter() {
        uniqueIdCounter += 1;
        return uniqueIdCounter;
    }

    // **************************************************
    // Getter methods
    // **************************************************
    public int getBookingId() {
        return this.bookingId;
    }

    public Time getStartTime() {
        return this.startTime;
    }

    public Time getEndTime() {
        return this.endTime;
    }

    // **************************************************
    // Setter methods
    // **************************************************
    public void setStartTime(Time newTime) {
        this.startTime = newTime;
    }

    public void setEndTime(Time newTime) {
        this.endTime = newTime;
    }

    /**
     * Function to shift booking object by time offset
     *
     * @return new Booking object with updated time
     */
    public Booking shiftBooking(boolean forward, int day, int hour, int minute) {
        return new Booking(this.getStartTime().shift(forward, day, hour, minute), this.getEndTime().shift(forward, day, hour, minute));
    }

    /**
     * Function to edit booking duration by time offset
     *
     * @return new Booking object with updated time
     */
    public Booking editBookingDuration(boolean forward, int day, int hour, int minute) {
        Time newEndTime = this.endTime.shift(forward, day, hour, minute);
        this.endTime.shift(!forward, day, hour, minute); //shift it back

        // If end time is before start time
        if (newEndTime.isBefore(this.startTime)) {
            System.out.println("new end time of booking earlier than start time!");
            return null;
        }

        // If move backward such that it overflows to next week
        if (forward && newEndTime.isBefore(this.startTime)) {
            System.out.println("New end time overflowed to next week!");
            return null;
        }

        // If move backward such that it overflows to previous week
        if (!forward && newEndTime.isAfter(this.startTime)) {
            System.out.println("New end time of overflowed to previous week!");
            return null;
        }

        return new Booking(this.getStartTime(), newEndTime);
    }


    /**
     * Function to do addition of booking timings without changing this booking variables
     *
     * @return new Booking object with updated time
     */
    public Booking add(boolean forward, int day, int hour, int minute) {
        Time newStartTime = this.getStartTime().shift(forward, day, hour, minute);
        Time newEndTime = this.getEndTime().shift(forward, day, hour, minute);

        if (newStartTime == null || newEndTime == null) {
            System.out.println("Booking add method: time shift operator out of proper range");
            // no need to shift back this object's start and end time becos the shift method would not do anything
            return null;
        } else {
            if (newEndTime.isBefore(newStartTime)) {
                // Shift back
                this.getStartTime().shift(!forward, day, hour, minute);
                this.getEndTime().shift(!forward, day, hour, minute);
                return null;
            }
            
            else {
                Booking temp = new Booking(newStartTime, newEndTime);
                this.getStartTime().shift(!forward, day, hour, minute);
                this.getEndTime().shift(!forward, day, hour, minute);
                return temp;
            }

        }

    }

}


