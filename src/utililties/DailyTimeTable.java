package src.utililties;

import java.util.*;

public class DailyTimeTable {
    Day day;
    List<Booking> bookings;

    // DailyTimeTable object constructor
    public DailyTimeTable(Day day) {
        this.day = day;
        this.bookings = new Vector<Booking>();
    }

    // **************************************************
    // Getter methods
    // **************************************************
    public Day getDay() {
        return this.day;
    }

    // Returns sorted bookings
    public List<Booking> getAllBookings() {
        Collections.sort(bookings, new Comparator<Booking>() {
            public int compare(Booking b1, Booking b2) {
                if (b1.getStartTime().isBefore(b2.getStartTime())) return -1;
                if (b1.getStartTime().isAfter(b2.getStartTime())) return 1;
                return 0;
            }
        });
        return this.bookings;
    }

    /**
     * Function to add booking to bookings list
     *
     * @return Boolean, true
     */
    public boolean addBooking(Booking booking) {
        this.bookings.add(booking);
        return true;
    }

    /**
     * Function to delete bookingId from bookings list
     *
     * @return Boolean, true if deleted, false if failed
     */
    public boolean deleteBooking(int bookingId) {
        Iterator<Booking> it = this.bookings.iterator();

        while (it.hasNext()) {
            Booking booking = it.next();
            if (booking.getBookingId() == bookingId) {
                it.remove();
                System.out.println("Deleted bookingId: " + bookingId);
                return true;
            }
        }
        System.out.println("failed to delete booking id: " + bookingId);
        return false;
    }
}
