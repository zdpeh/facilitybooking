package src.utililties;

import src.utililties.Day;
import java.text.DecimalFormat;

public class Time {
    Day day;
    int hour;
    int minute;

    // Time object constructor
    public Time(Day day, int hour, int minute) {
        this.day = day;
        this.hour = hour;
        this.minute = minute;
    }

    // returns stringified Time object in format DDDHHMM
    public String verbose() {
        return this.day.toString() + new DecimalFormat("00").format(this.hour) + new DecimalFormat("00").format(this.minute);
    }

    // **************************************************
    // Getter methods
    // **************************************************
    public Day getDay() {
        return this.day;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    // **************************************************
    // Setter methods
    // **************************************************
    public void setDay(Day day) {
        this.day = day;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    /**
     * Function to shift object time by offset
     *
     * @return new time value equal to current time + offset
     */
    public Time shift(boolean forward, int day, int hour, int minute) {
        if (day > 6 || hour > 23 || minute > 59 || day < 0 || hour < 0 || minute < 0) {
            System.out.println("Time shift method: time shift operator out of proper range!");
            return null;
        }

        // Postpone duration
        if (forward) {
            // Postpone minute
            // Minute overflow
            if (this.minute + minute > 59) {
                this.hour = this.hour + 1;
                this.minute = this.minute + minute - 60;

                // Hour overflow
                if (this.hour > 23) {
                    this.day = this.day.next(1);
                    this.hour = this.hour + hour - 24;
                }
            }
            // Minute no overflow
            else {
                this.minute = this.minute + minute;
            }

            // Postpone hour
            // Hour overflow in argument
            if (this.hour + hour > 23) {
                this.day = this.day.next(1);
                this.hour = this.hour + hour - 24;
            }
            // Hour no overflow
            else {
                this.hour = this.hour + hour;
            }

            // Postpone day
            this.day = this.day.next(day);

            return new Time(this.day, this.hour, this.minute);
        }

        // If earlier
        else {
            // Make earlier minute
            // Minute underflow
            if (this.minute - minute < 0) {
                this.hour = this.hour - 1;
                this.minute = this.minute - minute + 60;

                // Hour underflow
                if (this.hour < 0) {
                    this.day = this.day.previous(1);
                    this.hour = this.hour - hour + 24;
                }
            }
            // Minute no underflow
            else {
                this.minute = this.minute - minute;
            }

            // Make earlier hour
            // Hour overflow in argument
            if (this.hour - hour < 0) {
                this.day = this.day.previous(1);
                this.hour = this.hour - hour + 24;
            }
            // Hour no overflow
            else {
                this.hour = this.hour - hour;
            }

            // Make earlier day
            this.day = this.day.previous(day);

            return new Time(this.day, this.hour, this.minute);
        }

    }

    /**
     * Function to check if object's time is earlier than time
     *
     * @return boolean, True if earlier, False if later
     */
    public boolean isBefore(Time time) {
        if (this.getDay().ordinal() < time.getDay().ordinal()) {
            return true;
        } else if (this.getDay() == time.getDay() && this.getHour() < time.getHour()) {
            return true;
        } else if (this.getDay() == time.getDay() && this.getHour() == time.getHour() && this.getMinute() < time.getMinute()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function to check if object's time is later than time
     *
     * @return boolean, True if later, False if earlier
     */
    public boolean isAfter(Time time) {
        if (this.getDay().ordinal() > time.getDay().ordinal()) {
            return true;
        } else if (this.getDay() == time.getDay() && this.getHour() > time.getHour()) {
            return true;
        } else if (this.getDay() == time.getDay() && this.getHour() == time.getHour() && this.getMinute() > time.getMinute()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isEqual(Time time) {
        if (this.getDay() == time.getDay() && this.getHour() == time.getHour() && this.getMinute() == time.getMinute()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function to check if current time is between startTime and endTime
     *
     * @return boolean, True if between, False if outside
     */
    public boolean isBetween(Time startTime, Time endTime) {
        if (this.isAfter(startTime) && this.isBefore(endTime)) {
            return true;
        } else {
            return false;
        }
    }
}
