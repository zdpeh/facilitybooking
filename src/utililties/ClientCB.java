package src.utililties;

import java.net.*;
import java.util.Date;


public class ClientCB {
    private InetAddress clientAddress;
    private int clientPort;
    private Date expiry;

    public ClientCB(InetAddress clientAddress, int clientPort, Date expiry) {
        this.clientAddress = clientAddress;
        this.clientPort = clientPort;
        this.expiry = expiry;

    }

    public InetAddress getClientAddress() {
        return this.clientAddress;
    }

    public int getClientPort() {
        return this.clientPort;
    }

    public Date getExpiry() {
        return this.expiry;
    }

    public boolean isExpired() {
        return this.expiry.before(new Date());
    }

}
